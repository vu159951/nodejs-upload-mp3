import "reflect-metadata";
import {Request} from 'express'
import {
    Controller,
    Get,
    Post,
    Redirect,
    Req,
    UploadedFiles
} from "routing-controllers";
import * as path from "path";
import * as fs from "fs";
import * as request from "request";
import {MP3FileModel} from '../Model/MP3FileModel';
import invariant from "../utils/invariant";


const md5File = require('md5-file');

const horizon = require('horizon-youtube-mp3');

function onConvertVideoComplete(filename: string) {
    return async (err: any, result: any) => {
        const hash = md5File.sync('files/' + filename);
        const u = new MP3FileModel({filename: filename, hash: hash, migrated: false});
        await u.save();

        const formData = {
            file: fs.createReadStream('files/' + filename),
        };
        request.post({
            url: 'https://chordify.net/song/file:',
            formData: formData,
            headers: {
                'Cookie': '_ga=GA1.2.2010929228.1531909862; _gid=GA1.2.2065870364.1531909862; user=UuidqE%2BrBJwCP%2ForqzmTYq3Fsg%2BWPIhRCllkMewtYQg%3D; PHPSESSID=8pnghffah3jbgt31cr8ktjhb45',
                'Accept': 'application/json'
            }
        }, function (err, httpResponse, body) {
            if (err) {
                return {status: "Failed" + filename}
            }
            console.log('Upload successful!  Server responded with:', body);
            // fs.unlink('files/' + filename,function(err){
            //     if(err) return console.log(err);
            //     console.log('file deleted successfully');
            // });
        });
    }

}

function onConvertVideoProgress(percent: any, timemark: any, targetSize: any) {
    console.log('Progress:', percent, 'Timemark:', timemark, 'Target Size:', targetSize);
    // Will return...
    // Progress: 90.45518257038955 Timemark: 00:02:20.04 Target Size: 2189
    // Progress: 93.73001672942894 Timemark: 00:02:25.11 Target Size: 2268
    // Progress: 100.0083970106642 Timemark: 00:02:34.83 Target Size: 2420
}

@Controller()
export class YoutubeController {

    @Get("/youtube/save/:id")
    getOne(@Req() req: Request) {
        const downloadPath = 'files';
        const id = req.params.id;
        const removeAccents = require('remove-accents');
        try {
            horizon.getInfo('https://www.youtube.com/watch?v=' + id, function (err: any, data: any) {
                invariant(data.videoName, 'Invalid video');
                let fileName = data.videoName;
                fileName = removeAccents(fileName).replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, ''); //Remove todas as caracteres especiais e substitui letras com acentos para letras normais
                fileName = fileName + '.mp3';
                horizon.downloadToLocal(
                    'https://www.youtube.com/watch?v=' + id,
                    downloadPath,
                    fileName,
                    null,
                    900,
                    onConvertVideoComplete(fileName),
                    onConvertVideoProgress
                );
            });
        } catch(e) {
            return 'Error!';
        }




        return "Downloading!"
    }


}