import "reflect-metadata";
import {Request} from 'express'
import {
    Controller,
    Get,
    Post,
    Redirect,
    Req,
    UploadedFiles
} from "routing-controllers";
import * as multer from "multer";
import * as path from "path";
import * as fs from "fs";
import * as request from "request";
import {MP3FileModel} from '../Model/MP3FileModel';



const md5File = require('md5-file');

const config = require('config');

export const FILE_UPLOAD_OPTIONS = {
    storage: multer.diskStorage({
        destination: (req: any, file: any, cb: any) => {
            cb(null, 'files')
        },
        filename: (req: any, file: any, cb: any) => {
            cb(null, file.originalname)
        },

    }),
    limits: {
        fieldNameSize: 255,
        fileSize: 1024 * 1024 * 200
    },
    fileFilter: (req: any, file: any, cb: any) => {
        const filetypes = /ogg|mp3|wav/;
        const mimetype = filetypes.test(file.mimetype);
        const extname = filetypes.test(path.extname(file.originalname).toLowerCase());

        if (mimetype && extname) {
            return cb(null, true);
        }
        return cb('Only audio files are allowed!', false);
    },
};

@Controller()
export class FileController {

    @Get("/syncData")
    uploadToChordify(@Req() req: Request) {
        const urlPython = config.get('crawler.uri') + config.get('crawler.syncPath');
        request.get(urlPython);
        return {status: 'OK'}
    }

    @Post("/files")
    @Redirect('/')
    async handleFileUpload(@UploadedFiles("file", {options: FILE_UPLOAD_OPTIONS}) files: any) {
        for (let i = 0; i < files.length; i++) {
            const hash = md5File.sync('files/' + files[i].filename);
            try {
                const u = new MP3FileModel({filename: files[i].originalname.toString('utf8'), hash: hash, migrated: false });
                await u.save();

                const formData = {
                    file: fs.createReadStream('files/' + files[i].originalname),
                };
                request.post({
                    url: 'https://chordify.net/song/file:',
                    formData: formData,
                    headers: {
                        'Cookie': '_ga=GA1.2.2010929228.1531909862; _gid=GA1.2.2065870364.1531909862; user=UuidqE%2BrBJwCP%2ForqzmTYq3Fsg%2BWPIhRCllkMewtYQg%3D; PHPSESSID=8pnghffah3jbgt31cr8ktjhb45',
                        'Accept': 'application/json'
                    }
                }, function (err, httpResponse, body) {
                    if (err) {
                        return {status: "Failed" + files[i].originalname}
                    }
                    console.log('Upload successful!  Server responded with:', body);
                    // fs.unlink('files/' + files[i].originalname,function(err){
                    //     if(err) return console.log(err);
                    //     console.log('file deleted successfully');
                    // });
                });

            } catch (error) {
                console.error(error)
            }
        }

        return {
            status: "OK"
        }
    }

}