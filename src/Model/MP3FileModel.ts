import { prop, Typegoose } from 'typegoose';
import * as mongoose from 'mongoose';
const config = require('config');

mongoose.connect(buildMongoUri(config.get('dbConfig')));

function buildMongoUri(dbConfig: any) {
    return 'mongodb://' + dbConfig.host + ':' + dbConfig.port + '/' + dbConfig.dbName;
}


class MP3File extends Typegoose {
    @prop()
    filename?: string;
    @prop()
    hash?: string;
    @prop()
    migrated?: boolean;
}

const MP3FileModel = new MP3File().getModelForClass(MP3File);
export { MP3FileModel };

