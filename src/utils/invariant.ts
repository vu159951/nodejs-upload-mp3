export default function invariant(condition: any, msg: any) {
    if (!condition) {
        throw new Error(msg);
    }
}
