import "reflect-metadata"; // this shim is required
import {createExpressServer, useExpressServer} from "routing-controllers";
import {YoutubeController} from "./src/Controller/YoutubeController";
const express = require('express');
const serveIndex = require('serve-index');
const config = require('config');
const horizon = require('horizon-youtube-mp3');


process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";


// creates express app, registers all controller routes and returns you express app instance
const app = express();

app.get('/api/youtube/download/:id', function (req: any, res: any) {
    horizon.download('https://www.youtube.com/watch?v=' + req.params.id, res, null, null, null, false, function(err: any, e: any){

        if(err) {
            return console.log(err);
        }

        if(e === horizon.successType.CONVERSION_FILE_COMPLETE){
            console.log(e);
        }
    });
});

useExpressServer(app, {
    controllers: [__dirname + "/src/Controller/*.js"],
    routePrefix: "/api",
});

console.log(config);
app.engine('html', require('ejs').renderFile);

app.use('', express.static('files'), serveIndex('files', {'icons': false, 'template': 'views/index.html'}))

app.listen(80);
