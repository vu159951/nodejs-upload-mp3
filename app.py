# -*- coding: utf8 -*-
from flask import Flask

app = Flask(__name__)

import requests  # $ pip install requests
import json
import glob
import hashlib
import os
import pymongo


def md5Checksum(filePath):
    with open(filePath, 'rb') as fh:
        m = hashlib.md5()
        while True:
            data = fh.read(8192)
            if not data:
                break
            m.update(data)
        return m.hexdigest()


@app.route("/uploadFilesToChordify")
def main():
    myclient = pymongo.MongoClient("mongodb://localhost:27017/")
    mydb = myclient["dev"]
    mycol = mydb["mp3files"]
    header = {
        'Cookie': '_ga=GA1.2.2010929228.1531909862; _gid=GA1.2.2065870364.1531909862; user=UuidqE%2BrBJwCP%2ForqzmTYq3Fsg%2BWPIhRCllkMewtYQg%3D; PHPSESSID=8pnghffah3jbgt31cr8ktjhb45',
        'Accept': 'application/json'}

    for data in mycol.find({"migrated": False}):
        requests.post('https://chordify.net/song/file:',
                      files={'file': open('files/' + data['filename'], "rb")}, verify=False,
                      headers=header)

        print 'done'
    return "Uploaded all files!"


if __name__ == "__main__":
    app.run(host='0.0.0.0')
