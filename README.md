# Chord Finder

Find any chords of various songs over internet.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
- Node 8 above
- Typescript
- MongoDB
```

### Installing

Step 1: Install dependencies
 - Run:  `npm install`

Step 2: Create DB name
 - For DEV: create db name, then edit config in config folder: config/development.json
 - For PRD: create db name, then edit config in config folder: config/production.json

Step 3: Run
 - Windows: `npm start`

Step 4: Go to
 - `http://localhost`

## Authors

* **Vu Tran** - *Initial work* - [vu159951](https://gitlab.com/vu159951/nodejs-upload-mp3)

See also the list of [contributors](https://gitlab.com/vu159951/nodejs-upload-mp3/graphs/master) who participated in this project.
